import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from "react-router-dom";
import { CartDropdownContainer, EmptyMessage, CartItems } from "./cart-dropdown.styles";
import { setIsCartOpen } from '../../store/cart/cart.action';
import { selectIsCartOpen } from '../../store/cart/cart.selector';
import Button from '../button/button.component';
import CartItem from "../cart-item/cart-item.component";
import { selectCartItems } from '../../store/cart/cart.selector';

const CartDropdown = () => {
  const cartItems = useSelector(selectCartItems);
  const navigate = useNavigate();
  const isCartOpen = useSelector(selectIsCartOpen);
  const dispatch = useDispatch();

  const toggleIsCartOpen = () => dispatch(setIsCartOpen(!isCartOpen));

  const goToCheckout = () => {
    navigate('/checkout');
    toggleIsCartOpen();
  }

  return (
    <CartDropdownContainer>
      <CartItems>
        {
          cartItems.length ? (
            cartItems.map((item) => (
            <CartItem key={item.id} cartItem={item} />
          ))) : 
          (
            <EmptyMessage>Your cart is empty</EmptyMessage>
          )
        }
      </CartItems>
      <Button onClick={goToCheckout}>CHECKOUT</Button>
    </CartDropdownContainer>
  )
}

export default CartDropdown;