import { CategoryPreviewContainer, CategoryTitle, CategoryPreviewItem } from "./category-preview.styles";
import ProductCard from '../product-card/product-card.component';
import { FC } from "react";
import { CategoryItem } from '../../store/categories/category.types';

type CategoryPreviewProps = {
  title: string;
  products: CategoryItem[];
}

const CategoryPreview: FC<CategoryPreviewProps> = ({ title, products }) => {

  return (
    <CategoryPreviewContainer>
      <h2>
          <CategoryTitle to={`/shop/${title.toLowerCase()}`}>{title.toUpperCase()}</CategoryTitle>
      </h2>
      <CategoryPreviewItem>
        {
          products
            .filter((_, idx) => idx < 4 )
            .map((product) => (
              <ProductCard key={product.id} product={product} />
            ))
        }
      </CategoryPreviewItem>
    </CategoryPreviewContainer>
  )
}

export default CategoryPreview;