import { CartItemContainer, ItemDetails, ItemName } from "./cart-item.styles";
import { CartItem as CartItemType } from "../../store/cart/cart.types";
import { FC } from "react";

type CarItemProps = {
  cartItem: CartItemType;
}

const CartItem: FC<CarItemProps>  = ({cartItem}) => {
  const { name, quantity, imageUrl, price } = cartItem;

  return (
    <CartItemContainer>
      <img src={imageUrl} alt={name} />
      <ItemDetails>
        <ItemName>{name}</ItemName>
        <span className="price">
          {quantity} x ${price}
        </span>
      </ItemDetails>
    </CartItemContainer>
  )
}

export default CartItem;