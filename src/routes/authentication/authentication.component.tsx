
import SignUpForm from '../../components/sign-up-form/sign-up-form.component';
import SignInForm from "../../components/sign-in-form/sign-in-form.component";
import { AuthenticationContainer } from './authentication.styles';
import { selectCurrentUser } from '../../store/user/user.selector';
import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';
import { Fragment } from 'react';

const Authentication = () => {
  const currentUser = useSelector(selectCurrentUser);

  return (
    <Fragment>
      {
        currentUser ? (
          <Navigate to="/shop" />
        )
        :
        (
          <AuthenticationContainer>
            <SignInForm />
            <SignUpForm />
          </AuthenticationContainer>
        )
      }
    </Fragment>
    
  );
}

export default Authentication;