import { takeLatest, put, all, call } from 'typed-redux-saga/macro';
import { USER_ACTION_TYPES } from './user.types';
import { signInSuccess, signInFailed, signUpSuccess, signUpFailed, signOutSuccess, signOutFailed, EmailSignInStart, SignUpStart, SignUpSuccess } from './user.action';
import { getCurrentUser, 
  createUserDocumentFromAuth, 
  signInWithGooglePopup, 
  signInAuthUserWithEmailAndPassword,
  createAuthUserWithEmailAndPassword,
  signOutUser,
  AdditionalInformation,
} from '../../utils/firebase/firebase.utils';
import { User } from 'firebase/auth';

/**
 * 
 * onCheckUserSessionSagas
 */
export function* getSnapshotFromUserAuth(userAuth: User, additionalDetails?: AdditionalInformation) {
  try{
    const userSnapshot = yield* call(createUserDocumentFromAuth, userAuth, additionalDetails);
    
    if(userSnapshot) {
      yield* put(signInSuccess({ id: userSnapshot.id, ...userSnapshot.data()}))
    }
  }
  catch(error) {
    yield* put(signInFailed(error as Error));
  }
}

export function* isUserAuthenticated() {
  try {
    const userAuth = yield* call(getCurrentUser);
    if(!userAuth) return;
    yield* call(getSnapshotFromUserAuth, userAuth);
  }
  catch(error) {
    yield* put(signInFailed(error as Error));
  }
}

export function* onCheckUserSession() {
  yield* takeLatest(USER_ACTION_TYPES.CHECK_USER_SESSION, isUserAuthenticated)
}


/**
 * 
 * onSignInWithGoogleSagas
 */

export function* signInWithGoogle() {
  try{
    const { user } = yield* call(signInWithGooglePopup);
    yield* call(getSnapshotFromUserAuth, user);
  }
  catch(error) {
    yield* put(signInFailed(error as Error))
  }
}

export function* onGoogleSignInStart() {
  yield* takeLatest(USER_ACTION_TYPES.GOOGLE_SIGN_IN_START, signInWithGoogle);
}

/**
 * 
 * onSignInWithEmailPasswordSagas
 */

export function* signInWithEmail({ payload: { email, password }}: EmailSignInStart){
  try{
    const userCredential = yield* call(signInAuthUserWithEmailAndPassword, email, password)
    if(userCredential) {
      const { user } = userCredential;
      yield* call(getSnapshotFromUserAuth, user);
    }
  }
  catch(error) {
    yield* put(signInFailed(error as Error))
  }
}

export function* onEmailSignInStart() {
  yield* takeLatest(USER_ACTION_TYPES.EMAIL_SIGN_IN_START, signInWithEmail)
}

/**
 * 
 * onSignUpSagas
 */

export function* signUp({ payload: { email, password, displayName }}: SignUpStart) {
  try {
    const userCredentials = yield* call(createAuthUserWithEmailAndPassword, email, password);
    if(userCredentials) {
      const { user } = userCredentials;
      yield* put(signUpSuccess(user, { displayName }))
    }
  }
  catch(error) {
    yield* put(signUpFailed(error as Error))
  }
}

export function* onSignupStart() {
  yield* takeLatest(USER_ACTION_TYPES.SIGN_UP_START, signUp)
}

export function* signInAfterSignUp({ payload: { user, additionalDetails }}: SignUpSuccess) {
  yield* call(getSnapshotFromUserAuth, user, additionalDetails)
}

export function* onSignUpSuccess() {
  yield* takeLatest(USER_ACTION_TYPES.SIGN_UP_SUCCESS, signInAfterSignUp)
}

/**
 * 
 * onSignUpSagas
 */

export function* signOut() {
  try {
    yield* call(signOutUser);
    yield* put(signOutSuccess());
  }
  catch(error) {
    yield* put(signOutFailed(error as Error))
  }
}

export function* onSignOutStart() {
  yield* takeLatest(USER_ACTION_TYPES.SIGN_OUT_START, signOut);
}


/**
 * All sagas
 */
export function* userSagas() {
  yield* all([
    call(onCheckUserSession), 
    call(onGoogleSignInStart), 
    call(onEmailSignInStart),
    call(onSignupStart),
    call(onSignUpSuccess),
    call(onSignOutStart),
  ]);
}